# High-Level Requirements Request #

## Overview ##

This issue is intended to gather high-level requirements from external parties to ensure that the project team has clear and actionable inputs for the planning and execution phases.
Please provide detailed responses to the following sections.

## Basic Information ##

- **Project Name:**
- **Priority (High/Medium/Low):**

## Requirements Overview ##

### Goal of the Project wrt. SatNOGS ###

- **Brief Description**

### Key Functionalities ###

1. **Functionality 1**
2. **Functionality 2**

### Success Criteria ###

- **Criteria 1**
- **Criteria 2**

### Constraints ###

- **Constraint 1**
- **Constraint 2**

### Timeline ###

- **Expected Start Date:**
- **Deadline:**

## Key people ###

- **Stakeholder 1**
  - **Role:**
- **Stakeholder 2**
  - **Role:**

## Additional Comments ##
